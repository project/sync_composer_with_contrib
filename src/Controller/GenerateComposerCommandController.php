<?php

namespace Drupal\sync_composer_with_contrib\Controller;

use Drupal\Core\Controller\ControllerBase;
use DrupalFinder\DrupalFinder;
use MCStreetguy\ComposerParser\Factory as ComposerParser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate Composer commands or sync between modules and composer.json.
 */
class GenerateComposerCommandController extends ControllerBase {

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Generate Composer commands based on composer.json, contrib modules.
   *
   * @return string
   *   Return composer commands.
   */
  public function getComposerCommands() {
    // Attach necessary libraries.
    $build['#attached']['library'][] = 'sync_composer_with_contrib/sync_composer_with_contrib.commands_zone';

    $this->messenger()->addWarning($this->t('Please check if your contrib modules exist in drupal.org, as this process can decrease server performance.'));
    $this->messenger()->addWarning($this->t('This module is for development purposes to help you add and synchronize your contrib modules in composer.json.'));

    // Create the container for AJAX replacement.
    $build['container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'composer-container'],
    ];

    /*
     * Add all missing modules from contrib directory to composer.json.
     */
    $add_to_composer_all_missing_modules_from_contrib_directory = array_diff($this->getModulesFromDirectoryContrib(), $this->getModulesFromComposer());
    if (!empty($add_to_composer_all_missing_modules_from_contrib_directory)) {
      $renderable_add_to_composer_all_missing_modules_from_contrib_directory = [
        '#theme' => 'sync_composer_with_contrib_code_formatter',
        '#title' => $this->t('Add in composer.json all missing modules (installed and not installed)'),
        '#description' => $this->t('Add in composer.json all missing modules that exist in the contrib directory.'),
        '#composer_action' => 'require',
        // Use escapeshellarg() for security and ensure proper formatting.
        '#modules' => array_map(function($module) {
          $cleaned_module = trim($module, "' ");
          return escapeshellarg($cleaned_module);
        }, $add_to_composer_all_missing_modules_from_contrib_directory),
      ];
      $rendered_add_to_composer_all_missing_modules_from_contrib_directory = $this->renderer->renderPlain($renderable_add_to_composer_all_missing_modules_from_contrib_directory);
      $build['container']['add_to_composer_all_missing_modules_from_contrib_directory'] = [
        '#markup' => $rendered_add_to_composer_all_missing_modules_from_contrib_directory,
      ];
    }

    /*
     * Add only the installed modules to composer.json.
     */
    $add_to_composer_only_modules_installed = array_diff($this->getModulesContribInstalled(), $this->getModulesFromComposer());
    if (!empty($add_to_composer_only_modules_installed)) {
      $renderable_add_to_composer_only_modules_installed = [
        '#theme' => 'sync_composer_with_contrib_code_formatter',
        '#title' => $this->t('Add in composer.json only the missing modules installed'),
        '#description' => $this->t('Add in composer.json missing modules installed.'),
        '#composer_action' => 'require',
        // Use escapeshellarg() for security and ensure proper formatting.
        '#modules' => array_map(function($module) {
          $cleaned_module = trim($module, "' ");
          return escapeshellarg($cleaned_module);
        }, $add_to_composer_only_modules_installed),
      ];
      $rendered_add_to_composer_only_modules_installed = $this->renderer->renderPlain($renderable_add_to_composer_only_modules_installed);
      $build['container']['add_to_composer_only_modules_installed'] = [
        '#markup' => $rendered_add_to_composer_only_modules_installed,
      ];
    }

    /*
     * Remove modules from composer.json that exist but are not installed.
     */
    $module_in_composer_and_contrib = array_intersect($this->getModulesFromDirectoryContrib(), $this->getModulesFromComposer());
    $remove_unnecessary_modules = array_diff($module_in_composer_and_contrib, $this->getModulesContribInstalled());
    if (!empty($remove_unnecessary_modules)) {
      $renderable_remove_unnecessary_modules = [
        '#theme' => 'sync_composer_with_contrib_code_formatter',
        '#title' => $this->t('Remove from composer.json unnecessary modules'),
        '#description' => $this->t('Remove from composer.json unnecessary modules that exist in composer.json and contrib directory but are not installed.'),
        '#composer_action' => 'remove',
        // Use escapeshellarg() for security and ensure proper formatting.
        '#modules' => array_map(function($module) {
          $cleaned_module = trim($module, "' ");
          return escapeshellarg($cleaned_module);
        }, $remove_unnecessary_modules),
      ];
      $rendered_remove_unnecessary_modules = $this->renderer->renderPlain($renderable_remove_unnecessary_modules);
      $build['container']['remove_unnecessary_modules'] = [
        '#markup' => $rendered_remove_unnecessary_modules,
      ];
    }

    if (empty($add_to_composer_all_missing_modules_from_contrib_directory) && empty($add_to_composer_only_modules_installed) && empty($remove_unnecessary_modules)) {
      $build['no_results'] = ['#markup' => $this->t('There are no modules in the contrib directory.')];
    }

    return $build;
  }

  /**
   * Get contrib modules from contrib directory.
   *
   * @param string $dir
   *   Get the directory to search for modules.
   *
   * @return array
   *   Return list of modules in the selected directory.
   */
  public function getModulesFromDirectoryContrib($dir = 'modules/contrib') {
    $directories = glob($dir . '/*', GLOB_ONLYDIR);
    $dirs = [];
    foreach ($directories as $directory) {
      $module_name = explode('/', $directory)[2];
      $dirs[] = $module_name;
    }
    return $dirs;
  }

  /**
   * Get modules from the "require" section in composer.json.
   *
   * @return array
   *   Get modules from the "require" section in composer.json.
   */
  public function getModulesFromComposer() {
    $drupalFinder = new DrupalFinder();
    if ($drupalFinder->locateRoot(getcwd())) {
      $drupalRoot = $drupalFinder->getDrupalRoot();
      $composerRoot = $drupalFinder->getComposerRoot();
      $path_composer_json = $composerRoot . '/composer.json';
    }
    $composerJson = ComposerParser::parseComposerJson($path_composer_json);
    $data = $composerJson->getRequire()->getData();
    $modules = array_filter(array_keys($data), function ($v) {
      return substr($v, 0, 7) === 'drupal/';
    });
    $modules_final = [];
    foreach ($modules as $module) {
      // Escape the module name before adding it to the final list.
      $modules_final[] = escapeshellarg(explode('/', $module)[1]);
    }
    return $modules_final;
  }

  /**
   * Get list of contrib modules from the contrib directory that are installed.
   */
  public function getModulesContribInstalled() {
    $all_extensions = $this->moduleHandler->getModuleList();

    $contrib_modules = [];
    foreach ($all_extensions as $extension) {
      $pathname = $extension->getPathname();
      $explode = explode('/', $pathname);
      $module = $explode[2];
      $count_pathname = count($explode);
      if (substr_count($pathname, '/contrib/') === 1 && $count_pathname <= 4) {
        $contrib_modules[] = $module;
      }
    }
    return $contrib_modules;
  }

}
