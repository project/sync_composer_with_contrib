CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module return composer commands to add/remove modules in/from composer.json

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/sync_composer_with_contrib

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/sync_composer_with_contrib


REQUIREMENTS
------------

This module depends on "mcstreetguy/composer-parser" library.



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Mohamed Anis Taktak (matio89) - https://www.drupal.org/u/matio89

This project has been sponsored by:
 * MAT-IT
    A Tunisian web agency founded in 2016 and specialized in Drupal. We are
    based in Ariana, Tunisia and we work to provide ergonomic, clean
    and well designed Drupal websites. Please visit: https://www.mat-it.tech
    for further information.
